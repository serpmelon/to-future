const electron = require('electron')
const { ipcRenderer } = electron

//set background
const imgs = [
    'background_1.jfif',
    'background_2.png',
    'background_3.jpg',
    'background_4.jfif',
    'background_5.jfif',
]
const randomIndex = parseInt(Math.random() * 5)
document.querySelector('.background').setAttribute('src', `./img/${imgs[randomIndex]}`)

ipcRenderer.on('setTask', (event, task) => {
    document.querySelector('.reminder').innerHTML =
        `<span>${decodeURIComponent(task)}</span>的时间到啦！`
})

const closeDom = document.querySelector('.close')
closeDom.addEventListener('click', () => {
    // ipcRenderer.send('remindWindow:close')
    window.mBus.closeRemindWindow()
})