const { ipcRenderer, contextBridge } = require('electron')

contextBridge.exposeInMainWorld('mBus', {
    setTaskTimer: (name, date, time) => {
        ipcRenderer.send('setTaskTimer', date, time, encodeURIComponent(name))
    },
    closeMainWindow: () => {
        ipcRenderer.send('mainWindow:close')
    },
    closeRemindWindow: () => {
        ipcRenderer.send('remindWindow:close')
    },

    remindTask: (task, element) => {
        ipcRenderer.on('setTask', (event, task) => {
            element.innerHTML =`<span>${decodeURIComponent(task)}</span>的时间到啦！`
        })
    }
})