const electron = require('electron')
const path = require('path')

const { app, BrowserWindow, ipcMain, Tray, Menu, screen, dialog } = electron
const { autoUpdater } = require('electron-updater')
const iconPath = path.join(__dirname, './src/img/K_round_solid.png')

let mainWindow
let tray
let remindWindow

app.on('ready', () => {
    //检查更新
    checkUpdate()

    mainWindow = new BrowserWindow({
        //  frame boolean (可选) - 设置为 false 时可以创建一个无边框窗口 默认值为 true。
        frame: true,
        // true可以调节窗口大小，false不可以
        resizable: true,
        width: 800,
        height: 600,
        
        // icon (NativeImage | string) (可选) - 窗口图标。 在 Windows 上推荐使用 ICO 图标来获得最佳的视觉效果, 默认使用可执行文件
        icon: iconPath,
        show: false,
        backgroundColor: '#fadaaa',
        webPreferences: {
            // backgroundThrottlingboolean (可选)-是否在页面成为背景时限制动画和计时器。 这也会影响到 Page Visibility API. 默认值为 true。
            backgroundThrottling: false,
            // nodeIntegration boolean (可选) - 是否启用Node integration. 默认值为 false.
            nodeIntegration: true,
            // contextIsolation boolean (可选) - 是否在独立 JavaScript 环境中运行 Electron API和指定的preload 脚本. 默认为 true。 
            // 预加载脚本所运行的上下文环境只能访问其自身专用的文档和全局窗口，其自身一系列内置的JavaScript (Array, Object, JSON, 等等) 也是如此，这些对于已加载的内容都是不可见的。 Electron API 将只在预加载脚本中可用，在已加载页面中不可用。 
            // 这个选项应被用于加载可能不被信任的远程内容时来确保加载的内容无法篡改预加载脚本和任何正在使用的Electron api。 该选项使用的是与Chrome内容脚本相同的技术。 你可以在开发者工具Console选项卡内顶部组合框中选择 'Electron Isolated Context'条目来访问这个上下文。
            contextIsolation: true,
            // __dirname表示当前执行脚本的路径.
            preload: path.join(__dirname, './preload.js')
        }
    })

    // 官方建议的优雅显示窗口的方法, 监听ready-to-show事件
    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    })

    mainWindow.loadURL(`file://${__dirname}/src/main.html`)
    mainWindow.removeMenu()
    tray = new Tray(iconPath)
    tray.setToolTip('Tasky')
    tray.on('click', () => {
        if (mainWindow.isVisible()) {
            mainWindow.hide()
        } else {
            mainWindow.show()
        }
    })
    tray.on('right-click', () => {
        const menuConfig = Menu.buildFromTemplate([
            {
                label: 'Quit',
                click: () => app.quit()
            }
        ])
        tray.popUpContextMenu(menuConfig)
    })

})

ipcMain.on('mainWindow:close', () => {
    console.log('mainWindow:close')
    mainWindow.hide()
})

ipcMain.on('remindWindow:close', () => {
    remindWindow.close()
})

ipcMain.on('setTaskTimer', (event, date, time, task) => {

    console.log('setTaskTimer', date, time, task)
    const now = new Date()
    const temp = new Date()

    temp.setDate(date)
    temp.setHours(time.slice(0, 2), time.slice(3), 0)
    const timeout = temp.getTime() - now.getTime()
    setTimeout(() => {
        createRemindWindow(task)
    }, timeout)
})

function createRemindWindow(task) {
    if (remindWindow) remindWindow.close()
    remindWindow = new BrowserWindow({
        height: 450,
        width: 360,
        resizable: false,
        frame: false,
        icon: iconPath,
        show: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            // preload: path.join(__dirname, './preload.js')
        }
    })
    remindWindow.removeMenu()
    const size = screen.getPrimaryDisplay().workAreaSize
    const { y } = tray.getBounds()
    const { height, width } = remindWindow.getBounds()
    const yPosition = process.platform === 'darwin' ? y : y - height
    remindWindow.setBounds({
        x: size.width - width,
        y: yPosition,
        height,
        width
    })
    remindWindow.setAlwaysOnTop(true)
    remindWindow.loadURL(`file://${__dirname}/src/remind.html`)
    remindWindow.show()
    remindWindow.webContents.send('setTask', task)
    remindWindow.on('closed', () => { remindWindow = null })
    setTimeout(() => {
        remindWindow && remindWindow.close()
    }, 50 * 1000)
}

function checkUpdate() {
    if (process.platform == 'darwin') {
        autoUpdater.setFeedURL('http://127.0.0.1:9005/darwin')
    } else {
        autoUpdater.setFeedURL('http://127.0.0.1:9005/win32')
    }
    autoUpdater.checkForUpdates()
    autoUpdater.on('error', (err) => {
        console.log(err)
    })
    autoUpdater.on('update-available', () => {
        console.log('found new version')
    })
    autoUpdater.on('update-downloaded', () => {
        dialog.showMessageBox({
            type: 'info',
            title: '应用更新',
            message: '发现新版本，是否更新？',
            buttons: ['是', '否']
        }).then((buttonIndex) => {
            if (buttonIndex.response == 0) {
                autoUpdater.quitAndInstall()
                app.quit()
            }
        })
    })
}

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
})